var gulp = require('gulp');
var data = require('gulp-data');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();


gulp.task('stylus', function () {
  gulp.src('./stylus/styles.styl')
  .pipe(stylus())
  .pipe(autoprefixer(
    {
      browsers:['last 2 versions','last 2 Chrome versions','ie 6-8','iOS 7','not ie <= 8','Firefox > 20','Firefox < 20']
    }
  ))
  .pipe(gulp.dest('./dist/css/'))
  .pipe(browserSync.stream());
});

gulp.task('pug', function buildHTML() {
  return gulp.src('./pug/index.pug')
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest('./dist/'))
  .pipe(browserSync.stream());
});

gulp.task('js', function(){
  gulp.src('./js/main.js')
  .pipe(gulp.dest('./dist/js/'))
  .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./dist/"
        },
        open:false
    });
});



gulp.task('default',['stylus','pug','js','browser-sync'],function(){
  gulp.watch('./stylus/**/*.styl',['stylus']);
  gulp.watch('./pug/*.pug',['pug']);
  gulp.watch('./js/*.js',['js']);
});
