$(function(){

  $('.Nav a').click(function(e){
    e.preventDefault();
    $('.Full').hide();
    $('.Nav a').removeClass('active');
    $(this).addClass('active');
    var link = $(this).attr('href');
    history.pushState(null, "",link);
    link = link.replace('#','.');
    var index = $('.Container-pagina'+link).index();
    $('.Container-paginas').css('left','-'+index*100+'%');
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){

      $('.Header').removeClass('active');
      $('.Franja').hide();
      $(link+' .Franja').hide();
      $(link+' .Full').show('clip', {direction: 'vertical'});
      $('.Products-lista').show();
      $('.Products-contenido').hide();
      if (link=='.Home') {
        $('.HeaderMobile').show();
        $('.Lateral, .Logo-mobile').hide();
        $('.Header').removeClass('active');
        $('.Footer').removeClass('PaddingMobile');
        $('.Container-pagina.Home').removeClass('PaddingMobileHome');
      }
    }else{
      $('.Franja').show();
    }
  });

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    if (!Modernizr.csscalc) {
      var height = screen.height - 52;
      $('.Container-interno').css('min-height',height+'px');
    }
  }else{
    $(".About-cart").flip({
      trigger: 'hover'
    });
  }

  $('.Franja-imagen').click(function(){
    var link = $(this).attr('data-toggle');
    $('.'+link+' .Franja').hide();
    $('.'+link+' .Full').show('clip', {direction: 'vertical'});
    $('.Products-lista').show();
    $('.Products-contenido').hide();
  });

  $('.Full-minimiza').click(function(){
    var link = $(this).attr('data-toggle');
    $('.'+link+' .Full').hide('clip', {direction: 'vertical'});
    $('.'+link+' .Franja').show('scale');
  });

  $('.Products-lista li a').click(function(){
    $('.Products-lista').hide('clip', {direction: 'vertical'});
    $('.Products-contenido').show('clip', {direction: 'vertical'});
    $('.flexslider').flexslider({
       animation: "slide",
       directionNav:false
     });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      $('.Full-minimiza-products').show();
    }
  });

  $('.Full-minimiza-products').click(function(){
    $('.Products-lista').show('clip', {direction: 'vertical'});
    $('.Products-contenido').hide('clip', {direction: 'vertical'});
    $('.Full-minimiza-products').hide();
  });

  $('.Container-paginas').bind('mousewheel', function(e){
    $('.Full').hide();
    $('.Franja').show();
    var porcentaje = $('.Container-paginas')[0].style.left;
    if (porcentaje == "") {
      porcentaje = '0%';
    }
    porcentaje = parseFloat(porcentaje.replace('%',''));
    if(e.originalEvent.wheelDelta /120 > 0) {
      if (porcentaje == 0 || porcentaje > -500) {
        $('.Container-paginas').css('left',(porcentaje - 100)+'%');
      }else if(porcentaje == -500){
        $('.Container-paginas').css('left','0%');
      }
    }else{
      if (porcentaje < 0  && porcentaje >= -500) {
        $('.Container-paginas').css('left',(porcentaje + 100)+'%');
      }else if(porcentaje == 0){
        $('.Container-paginas').css('left','-500%');
      }
    }

    var porcentaje = $('.Container-paginas')[0].style.left;
    porcentaje = parseFloat(porcentaje.replace('%',''));
    if (porcentaje == "") {
      porcentaje = 0;
    }
    $('.Nav a').removeClass('active');
    if (porcentaje==0) {
      $('.Nav a.home').addClass('active');
      history.pushState(null, "",'#Home');
    }else if (porcentaje == -100) {
      $('.Nav a.about').addClass('active');
      history.pushState(null, "",'#About');
    }else if (porcentaje == -200) {
      $('.Nav a.products').addClass('active');
      history.pushState(null, "",'#Products');
    }else if (porcentaje == -300) {
      $('.Nav a.materials').addClass('active');
      history.pushState(null, "",'#Materials');
    }else if (porcentaje == -400) {
      $('.Nav a.corporate').addClass('active');
      history.pushState(null, "",'#Corporate');
    }else if (porcentaje == -500) {
      $('.Nav a.contact').addClass('active');
      history.pushState(null, "",'#Contact');
    }
  });


  /*=============================================>>>>>
  = IDENTIFICA HASH Y UBICA PAGINA =
  ===============================================>>>>>*/
  var hash = window.location.hash;
  if (hash == '#About' || hash == '#About' || hash == '#Products' || hash == '#Materials' || hash == '#Clients' || hash == '#Corporate' || hash == '#Contact') {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      $('.HeaderMobile').hide();
      $('.Lateral, .Logo-mobile').show();
      $('.Footer').addClass('PaddingMobile');
    }
    $('.Nav a').removeClass('active');
    if (hash=='#About') {
      $('.Nav a.about').addClass('active');
      $('.Container-paginas').css('left','-100%');
    }else if (hash=='#Products') {
      $('.Nav a.products').addClass('active');
      $('.Container-paginas').css('left','-200%');
    }else if (hash=='#Materials') {
      $('.Nav a.materials').addClass('active');
      $('.Container-paginas').css('left','-300%');
    }else if (hash=='#Corporate') {
      $('.Nav a.corporate').addClass('active');
      $('.Container-paginas').css('left','-400%');
    }else if (hash=='#Contact') {
      $('.Nav a.contact').addClass('active');
      $('.Container-paginas').css('left','-500%');
    }
    hash = hash.replace('#','.');
    $(hash+' .Full').show();
    $('.Products-lista').show();
    $('.Products-contenido').hide();
  }
  /*= End of IDENTIFICA HASH Y UBICA PAGINA =*/
  /*=============================================<<<<<*/


  /*=============================================>>>>>
  = MENU MOBILE =
  ===============================================>>>>>*/

  $('.HeaderMobile-ToggleMenu').click(function(){
    $('.HeaderMobile').hide();
    $('.Lateral, .Logo-mobile').show();
    $('.Header').addClass('active');
    $('.Footer').addClass('PaddingMobile');
    $('.Container-pagina.Home').addClass('PaddingMobileHome');
  });

  $('.Lateral-hamburguer').click(function(){
    $('.Header').toggleClass('active');
  });

  $('.Logo-mobile').click(function(){
    history.pushState(null, "",'#Home');
    $('.Container-paginas').css('left','0%');
    $('.Logo-mobile').hide();
    $('.HeaderMobile').show();
    $('.Lateral, .Logo-mobile').hide();
    $('.Header').removeClass('active');
    $('.Footer').removeClass('PaddingMobile');
    $('.Container-pagina.Home').removeClass('PaddingMobileHome');
  });


  /*= End of MENU MOBILE =*/
  /*=============================================<<<<<*/


  /*=============================================>>>>>
  = VALIDACION DE FORMULARIO CONTACT US =
  ===============================================>>>>>*/
  $.validator.setDefaults({
		errorPlacement: function (error, element) { // render error placement for each input type

		},
		highlight: function (element) {
			$(element).addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).removeClass('has-error');
		}
	});

  $('.Contact-form').validate({
		rules: {
			firstName:{required:true},
			lastName:{required:true},
			email:{required:true,email:true},
			message:{required:true}
		},
		// messages: {
		// 	firstName:{required:'Este campo es requerido'},
		// 	lastName:{required:'Este campo es requerido',email:'Ingrese un Email valido'},
		// 	email:{required:'Este campo es requerido'},
		// 	message:{required:'Este campo es requerido'}
		// },
		submitHandler:function(form) {
			// $('#FormContacto').ajaxSubmit({
			// 	success : function(data){
			// 		if (data==='ok') {
			// 			swal(
			// 				'Buen trabajo!',
			// 				'Se envió correctamente el formulario.',
			// 				'success'
			// 			)
			// 			$('#FormContacto')[0].reset();
			// 			generarCaptcha();
			// 		}else{
			// 			swal(
			// 				'Algo salio mal!',
			// 				'Porfavor intentelo nuevamente mas tarde.',
			// 				'error'
			// 			)
			// 		}
			// 	}
			// });
		}
	});
  /*= End of VALIDACION DE FORMULARIO CONTACT US =*/
  /*=============================================<<<<<*/


});
